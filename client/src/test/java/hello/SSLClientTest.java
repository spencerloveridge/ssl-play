package hello;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class SSLClientTest {

    @BeforeClass
    public static void initTrustStore() {
        System.setProperty("javax.net.debug","all");
        System.setProperty("javax.net.ssl.trustStore","src/test/resources/custom.truststore");
        System.setProperty("server.ssl.trust-store-password","trustchangeme");

    }

    @Test
    public void shouldSendSecureMessage() throws IOException {
        String message = "Random-" + UUID.randomUUID();
        String url = "https://localhost:8443/message";

        HttpPost postRequest = new HttpPost(url);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        postRequest.setEntity(new StringEntity(message));
        CloseableHttpResponse response = httpClient.execute(postRequest);
        dumpResponse(response);

        HttpGet getRequest = new HttpGet(url);
        response = httpClient.execute(getRequest);
        assertEquals(message, EntityUtils.toString( response.getEntity() ) );
    }

    protected void dumpResponse(CloseableHttpResponse response) throws IOException
    {
        String json = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        while ((line = br.readLine()) != null)
            System.out.println(line);
        br.close();
    }
}
