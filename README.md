# SSL Play

This project demonstrates the process of how to setup secure HTTPS subscriber endpoint and the client code to call it. The "server" is the subscribing endpoint implementation that will receive the data. The "client" is the service that makes the HTTPS post request 

## How it works

We want to demonstrate how we can communicate securely with HTTP endpoint subscribers via SSL without the use of CA trust stores. In this tutorial it uses an embedded tomcat server running within a Spring Boot app, but the subscriber implementation could be any web server that implements the SSL protocol.

The basic setup is as follows:

* The subscriber generates a certificate and gives it the client as a one off setup
* The client installs the certificate into the "truststore" which is a list of servers it trusts that can be either embedded in the JVM or supplied from an external file (as in this example)
* With the subscriber now supplying the certificate whenever the client makes a connection, the subscriber is authenticated by the client and vice versa and thus encrypted communication can commence.

This tutorial shows:

* How to generate the certificate
* How to install the certificate in keystore and have the tomcat server reference it
* How to export the certificate from the keystore in X509 format
* How to add the X509 certificate to the clients truststore in the JVM

## Prerequisities

* Java 1.8 installed
* Maven installed
* JDK bin on the path (so you can run the ```keytool``` command)

## Setting up a server (Subscriber)

* We need to generate the certificate key and keystore file for the server by running the following from a command bash prompt in ```/ssl-play```

```
#!python


keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore server/src/main/resources/keystore.p12 -validity 3650 -storepass 123456
```


* Enter the following (or similar) details when prompted:


```
#!

    What is your first and last name?
      [Unknown]:  localhost
    What is the name of your organizational unit?
      [Unknown]:  DEV
    What is the name of your organization?
      [Unknown]:  MyCompany
    What is the name of your City or Locality?
      [Unknown]:  London
    What is the name of your State or Province?
      [Unknown]:
    What is the two-letter country code for this unit?
      [Unknown]:  UK
    Is CN=localhost, OU=DEV, O=MyCompany, L=London, ST=Unknown, C=UK correct?
      [no]:  yes
```


* Rebuild and start the app:


```
#!

    cd server
    mvn clean install && java -jar target/ssl-server-0.1.0.jar

```

This will add the keystore to the resources and an application property points to this file to be the keystore. The keystore determines what certifcate is supplied to any incoming clinets making an incoming SSL connections

* You will need to export it as an x509 certificate. Run the following from ```/ssl-play```:


```
#!

keytool -export -alias tomcat -storetype PKCS12 -keystore server/src/main/resources/keystore.p12 -storepass 123456 -rfc -file X509_certificate.cer
```


## Setting up the client

* Then in the client import the key into the trust store:

    
```
#!

keytool -importcert -keystore client/src/test/resources/custom.truststore -alias tomcat -storepass trustchangeme -file X509_certificate.cer -noprompt
```


* Run the client tests and they should pass:


```
#!

cd client    
mvn clean test
```