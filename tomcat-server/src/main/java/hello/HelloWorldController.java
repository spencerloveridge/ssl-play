package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
public class HelloWorldController {

    @Autowired
    private HelloWorldService helloWorldService;

    @RequestMapping(value="/events",method=RequestMethod.POST)
    public String hellowWorld(@RequestBody(required = true) String data) {
        return helloWorldService.getHelloMessage();
    }

    @RequestMapping("/events-async")
    public Callable<String> helloWorldAsync() {
        return new Callable<String>() {

            @Override
            public String call() throws Exception {
                return "async: "
                        + HelloWorldController.this.helloWorldService.getHelloMessage();
            }

        };
    }

}
